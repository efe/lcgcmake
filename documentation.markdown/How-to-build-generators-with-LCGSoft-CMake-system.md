1. Clone GIT repository:

        git clone https://gitlab.cern.ch/sft/lcgcmake.git

2. Create build and install directories:

        mkdir lcgcmake-{build,install-opt}
        cd lcgcmake-build

3. Setup your environment (if needed)
    * `export FC=gfortran`  (actually this is needed only for slc5 platforms)
    * `export PATH=/afs/cern.ch/sw/lcg/external/CMake/3.2.3/Linux-i386/bin:$PATH`
    *  `source /afs/cern.ch/sw/lcg/external/gcc/4.8/x86_64-slc6-gcc48-opt/setup.sh` (this configures your gcc)

4. Prepare make files using CMake:

        cmake -DLCG_INSTALL_PREFIX=/afs/cern.ch/sw/lcg/releases/LCG_<release> -DLCG_VERSION=<release> -DCMAKE_INSTALL_PREFIX=../install -DPDFsets=minimal ../lcgcmake

    Advanced Example: building dev4 against latest nightly

        cmake -DLCG_INSTALL_PREFIX=/afs/cern.ch/sw/lcg/app/nightlies/dev4/$(date +%a) -DLCG_VERSION=dev4 -DCMAKE_INSTALL_PREFIX=../install -DPDFsets=minimal ../lcgcmake

    (for some explanations on the arguments see [Using LCGCMake](https://ph-dep-sft.web.cern.ch/document/using-lcgcmake) and [LCGCMake options](https://gitlab.cern.ch/sft/lcgcmake/wikis/LCGCMake-options))
    Other possible interesting arguments are:

    *`-DLCG_TARBALL_INSTALL=ON`

    *`-DPDFsets=ct10`

5. Build the binaries:

        make -j 8 <package>              # Build all versions of <package>
        make -j 8 <package>-<version>    # Build only specified version

6. Do tests (notice: doesn't work on AFS)

        ctest -R <package>
